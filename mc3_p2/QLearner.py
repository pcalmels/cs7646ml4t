"""
Template for implementing QLearner  (c) 2015 Tucker Balch
"""

import numpy as np
import random as rand
from collections import Counter
import sys
import time

class QLearner(object):

    def __init__(self, \
                 num_states=100, \
                 num_actions = 4, \
                 alpha = 0.2, \
                 gamma = 0.9, \
                 rar = 0.5, \
                 radr = 0.99, \
                 dyna = 0, \
                 verbose = False):

        self.num_states = num_states
        self.num_actions = num_actions
        self.s = 0
        self.a = 0
        self.alpha = alpha
        self.gamma = gamma
        self.rar = rar
        self.radr = radr
        self.dyna = dyna
        self.verbose = verbose
        self.Q = np.random.uniform(low=-1.0, high=1.0, size=(num_states, num_actions))

        # DYNA
        self.T = np.ones(shape=(self.num_states, self.num_actions, self.num_states))*1.0/self.num_states
        self.Tc = np.zeros(shape=(self.num_states, self.num_actions, self.num_states))
        self.R = np.zeros(shape=(self.num_states, self.num_actions))
        self.seen_transitions = []

    def querysetstate(self, s):
        """
        @summary: Update the state without updating the Q-table
        @param s: The new state
        @returns: The selected action
        """
        self.s = s
        action = np.argmax(self.Q[s, :])
        if np.random.uniform(0.0, 1.0) < self.rar:
            action = rand.randint(0, self.num_actions - 1)
        self.a = action
        if self.verbose: print "s =", s,"a =",action
        return action

    def query(self, s_prime, r):
        """
        @summary: Update the Q table and return an action
        @param s_prime: The new state
        @param r: The ne state
        @returns: The selected action
        """
        action = np.argmax(self.Q[s_prime, :])
        self.Q[self.s, self.a] = (1 - self.alpha) * self.Q[self.s, self.a] + \
                                 self.alpha * (r + self.gamma * self.Q[s_prime, action])

        self.Tc[self.s, action, s_prime] += 1
        self.seen_transitions.append([self.s, self.a, s_prime])
        self.T[self.s, action, :] = self.Tc[self.s, action, :] / np.sum(self.Tc[self.s, action, :])
        self.R[self.s, action] = (1-self.alpha)*self.R[self.s, action]+self.alpha*r

        pairs = np.random.randint(0, len(self.seen_transitions), size=self.dyna)
        for i in pairs:
            s = self.seen_transitions[i][0]
            a = self.seen_transitions[i][1]
            new_sprime = self.seen_transitions[i][2]
            self.Q[s, a] = (1 - self.alpha) * self.Q[s, a] + \
                           self.alpha * (self.R[s, a] + self.gamma * np.max(self.Q[new_sprime, :]))

        if np.random.uniform(0.0, 1.0) < self.rar:
            action = rand.randint(0, self.num_actions - 1)
        self.rar *= self.radr
        self.a = action
        self.s = s_prime
        if self.verbose: print "s =", s_prime,"a =",action,"r =",r
        return action

if __name__=="__main__":
    print "Remember Q from Star Trek? Well, this isn't him"
