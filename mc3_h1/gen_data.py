"""
template for generating data to fool learners (c) 2016 Tucker Balch
"""

import numpy as np
import math

# this function should return a dataset (X and Y) that will work
# better for linear regression than random trees
def best4LinReg():
    X = np.random.normal(size=(900, 4))
    Y = X[:, 0] + 2 * np.square(X[:, 1]) + 3 * np.power(X[:, 2], 3) + 4 * np.power(X[:, 3], 3)
    return X, Y

def best4RT():
    X = np.random.normal(size=(900, 900))
    Y = np.array(range(900))
    return X, Y

if __name__=="__main__":
    print "they call me Tim."
