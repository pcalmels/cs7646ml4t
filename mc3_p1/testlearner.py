"""
Test a learner.  (c) 2015 Tucker Balch
"""

import numpy as np
import math
import LinRegLearner as lrl
import sys
import RTLearner as rt
import BagLearner as bl
import time

if __name__=="__main__":
    if len(sys.argv) != 2:
        print "Usage: python testlearner.py <filename>"
        sys.exit(1)
    inf = open(sys.argv[1])
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])

    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]

    print testX.shape
    print testY.shape

    for i in range(10,50,10):
        results_list = np.zeros((5,7))
        for j in range(5):
        # # create a learner and train it
        # learner = lrl.LinRegLearner(verbose = True) # create a LinRegLearner
        #     learner = rt.RTLearner(leaf_size=1, verbose=False)  # constructor
            start = time.clock()
            learner = bl.BagLearner(rt.RTLearner, {"leaf_size": i}, 15, False, False)

            learner.addEvidence(trainX, trainY) # train it

            # evaluate in sample
            predY = learner.query(trainX) # get the predictions


            rmse1 = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
            # print
            # print "In sample results"
            # print "RMSE: ", rmse
            c1 = np.corrcoef(predY, y=trainY)
            # print "corr: ", c[0,1]

            # evaluate out of sample
            predY = learner.query(testX) # get the predictions

            rmse2 = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
            # print
            # print "Out of sample results"
            # print "RMSE: ", rmse
            c2 = np.corrcoef(predY, y=testY)
            # print "corr: ", c[0,1]
            print(str(i)+", " + "15, "+str(rmse1)+", "+str(c1[0, 1]) +", "+str(rmse2)+", "+str(c2[0, 1])
                  + ", NaN, " + str((time.clock() - start)*1000))
            results_list[j, :] = np.array([i, 15, rmse1, c1[0, 1], rmse2, c2[0, 1], (time.clock()-start)*1000])
        r_mean = np.mean(results_list, axis=0)
        print "--- mean ---"
        print(str(r_mean[0]) + ", " + str(r_mean[1]) + ", " + str(r_mean[2]) + ", " + str(r_mean[3]) + ", " + str(r_mean[4]) + ", " + str(r_mean[5])
              + ", NaN, " + str(r_mean[6]))
        print "--- ---- ---"

