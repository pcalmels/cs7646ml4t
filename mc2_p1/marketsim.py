"""MC2-P1: Market simulator."""

import pandas as pd
import numpy as np
import datetime as dt
import os
from util import get_data, plot_data

def compute_portvals(orders_file = "./orders/orders.csv", start_val = 1000000):
    # this is the function the autograder will call to test your code
    orders_df = pd.read_csv(orders_file, index_col='Date', parse_dates=True, na_values=['nan']).dropna()
    date_index = orders_df.index.unique()
    symbols = orders_df['Symbol'].unique().tolist()
    prices_df = get_data(symbols, pd.date_range(date_index[0], date_index[-1]))
    prices_df = prices_df[symbols]  # remove SPY
    prices_df['CASH'] = np.ones(prices_df.shape[0])

    book_df = prices_df.copy()
    book_df[:] = np.zeros(prices_df.shape)
    for index, row in orders_df.iterrows():
        shares = int(row['Shares'])
        symb = row['Symbol']
        # Compute leverage
        if row['Order'] == "SELL":
            shares *= -1
        book_df.ix[index][symb] += shares
        book_df.ix[index]['CASH'] += -1*prices_df.ix[index][symb]*shares

    holdings_df = book_df.copy()
    holdings_df.ix[0]['CASH'] = start_val + book_df.ix[0].CASH
    prec_index = date_index[0]
    for index, row in holdings_df[1:].iterrows():
        new_row = (holdings_df.ix[prec_index] + book_df.ix[index])*prices_df.ix[index]
        leverage = new_row[:-1].abs().sum() / new_row.sum()
        if leverage < 3.0:
            holdings_df.ix[index] = holdings_df.ix[prec_index] + book_df.ix[index]
        else:
            holdings_df.ix[index] = holdings_df.ix[prec_index]
        prec_index = index

    portval = prices_df*holdings_df
    return portval.sum(axis=1)

def test_code():
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters

    of = "./orders/orders-leverage-2.csv"
    sv = 1000000

    # Process orders
    portvals = compute_portvals(orders_file = of, start_val = sv)
    if isinstance(portvals, pd.DataFrame):
        portvals = portvals[portvals.columns[0]] # just get the first column
    else:
        "warning, code did not return a DataFrame"
    
    # Get portfolio stats
    # Here we just fake the data. you should use your code from previous assignments.
    start_date = portvals.index[0]
    end_date = portvals.index[-1]
    # ok
    cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = get_portfolio_stats(portvals)
    # not ok
    cum_ret_SPY, avg_daily_ret_SPY, std_daily_ret_SPY, sharpe_ratio_SPY = [0.2, 0.01, 0.02, 1.5]

    # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(start_date, end_date)
    print
    print "Sharpe Ratio of Fund: {}".format(sharpe_ratio)
    print "Sharpe Ratio of SPY : {}".format(sharpe_ratio_SPY)
    print
    print "Cumulative Return of Fund: {}".format(cum_ret)
    print "Cumulative Return of SPY : {}".format(cum_ret_SPY)
    print
    print "Standard Deviation of Fund: {}".format(std_daily_ret)
    print "Standard Deviation of SPY : {}".format(std_daily_ret_SPY)
    print
    print "Average Daily Return of Fund: {}".format(avg_daily_ret)
    print "Average Daily Return of SPY : {}".format(avg_daily_ret_SPY)
    print
    print "Final Portfolio Value: {}".format(portvals[-1])



def get_portfolio_value(prices, allocs, start_val):
    normed = prices/prices.ix[0]
    allocated = normed*allocs
    pos_value = allocated*start_val
    portfolio_value = pos_value.sum(axis=1)
    return portfolio_value


def get_portfolio_stats(port_val, daily_rf=0.0, samples_per_year=252):
    daily_return = port_val.copy()
    daily_return.ix[1:] = (port_val.ix[1:]/port_val.ix[:-1].values - 1)
    daily_return.ix[0] = 0
    daily_return_normed = daily_return.ix[1:]

    cr = (port_val[-1]/port_val[0])-1
    adr = daily_return_normed.mean()
    sddr = daily_return_normed.std()
    sr = np.sqrt(samples_per_year)*(adr-daily_rf)/sddr
    return cr, adr, sddr, sr


def plot_normalized_data(df, title, xlabel, ylabel):
    df_normed = df/df.ix[0, :]
    plot_data(df_normed, title, xlabel, ylabel)


if __name__ == "__main__":
    test_code()
