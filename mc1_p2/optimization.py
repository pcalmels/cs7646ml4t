"""MC1-P2: Optimize a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize
import datetime as dt
from util import get_data, plot_data


# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # find the allocations for the optimal portfolio
    # note that the values here ARE NOT meant to be correct for a test case
    allocs = optimize_allocs(prices, syms)  # add code here to find the allocations
    port_val, cr, adr, sddr, sr = get_portfolio_stats(prices, allocs, 0.0, 252)  # add code here to compute stats

    # Get daily portfolio value
    # port_val = get_portfolio_value(prices, allocs)  # add code here to compute daily portfolio values

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)
        plot_normalized_data(df_temp, "Daily portfolio value and SPY", "Date", "Normalized price")
        pass

    return allocs, cr, adr, sddr, sr


def optimize_allocs(prices, syms):
    number_of_stocks = len(syms)
    # allocs = [0.25, 0.25, 0.25, 0.25]
    allocs = (np.ones(number_of_stocks) * 1.00 / number_of_stocks)

    def invert_sr(allocations):
        normed = prices / prices.ix[0]
        allocated = normed * allocations
        portfolio_value = allocated.sum(axis=1)

        daily_return = portfolio_value.copy()

        daily_return.ix[1:] = (portfolio_value.ix[1:] / portfolio_value.ix[:-1].values - 1)
        daily_return.ix[0] = 0
        daily_return_normed = daily_return.ix[1:]
        return -np.sqrt(252) * daily_return_normed.mean() / daily_return_normed.std()

    # new_allocs = allocs.copy()
    bound_tuple = (0, 1)
    bounds = tuple(bound_tuple for i in range(number_of_stocks))
    constraints = ({'type': 'eq', 'fun': lambda x: 1.0-np.sum(x)})
    return minimize(invert_sr,
                    allocs,
                    method='SLSQP',
                    bounds=bounds,
                    constraints=constraints).x


def get_portfolio_value(prices, allocs):
    normed = prices / prices.ix[0]
    allocated = normed * allocs
    port_val = allocated.sum(axis=1)
    return port_val


def get_portfolio_stats(prices, allocs, daily_rf, samples_per_year):
    port_val = get_portfolio_value(prices, allocs)
    daily_return = port_val.copy()
    daily_return.ix[1:] = (port_val.ix[1:]/port_val.ix[:-1].values - 1)
    daily_return.ix[0] = 0
    daily_return_normed = daily_return.ix[1:]

    cr = (port_val[-1]/port_val[0])-1
    adr = daily_return_normed.mean()
    sddr = daily_return_normed.std()
    sr = np.sqrt(samples_per_year)*(adr-daily_rf)/sddr
    return port_val, cr, adr, sddr, sr


def plot_normalized_data(df, title, xlabel, ylabel):
    df_normed = df/df.ix[0, :]
    plot_data(df_normed, title, xlabel, ylabel)


def test_code():
    # This function WILL NOT be called by the auto grader
    # Do not assume that any variables defined here are available to your function/code
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!

    start_date = dt.datetime(2008, 1, 1)
    end_date = dt.datetime(2009, 12, 31)
    symbols = ['IBM', 'X', 'HNZ', 'XOM', 'GLD']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = False)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    # This code WILL NOT be called by the auto grader
    # Do not assume that it will be called
    test_code()
