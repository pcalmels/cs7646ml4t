#!/usr/bin/python
import getopt
import os
import sys
import pandas as pd
import datetime as dt
import util as ut
import CTLearner as ctl
import BagLearner as bgl
import ML_based as ml
import indicators as ind
import numpy as np
import rule_based


_author_ = "Pierrick Calmels"


def main(argv):
    strategy = 'all'
    verbose = False
    save_orders = False
    try:
        opts, args = getopt.getopt(argv, "hs:v:o:", ["strategy=", "verbose=", "orders="])
    except getopt.GetoptError:
        print('launch.py -s/--strategy <"all", "rule", "ml", default all> -v/--verbose <prints nothing if false> -o/--orders <if True, saves the orders into a file>')
        sys.exit(2)
    for opt, arg in opts:
        print(opt)
        if opt == '-h':
            print('launch.py -s/--strategy <"all", "rule", "ml", default all> -v/--verbose <prints nothing if false> -o/--orders <if True, saves the orders into a file>')
            sys.exit()
        elif opt in ("-s", "--strategy"):
            strategy = arg
        elif opt in ("-v", "--verbose"):
            try:
                verbose = arg == 'True'
            except ValueError:
                print('wrong v, should be False or True, False used')
        elif opt in ("-o", "--orders"):
            try:
                save_orders = arg == 'True'
            except ValueError:
                print('wrong v, should be False or True, False used')
    return strategy, verbose, save_orders


def run_strategy(strategy, verbose, save_orders):
    # TRAINING
    # set parameters for training the learner
    stdate = dt.datetime(2006, 1, 1)
    enddate = dt.datetime(2009, 12, 31)  # just a few days for "shake out"
    syms = ["IBM"]

    dates = pd.date_range(start=stdate, end=enddate)
    prices = ut.get_data(syms, dates)  # automatically adds SPY
    # instantiate the strategy learner
    indicators = ind.Indicators(14)
    indicators.train(prices, syms, verbose=verbose)
    if verbose: print prices
    if strategy == "all" or strategy == "rule" :
        rb_df, rb_orders = rule_based.rb_strategy(indicators, prices, syms, verb=verbose)
        if verbose or save_orders:
            order_list = []
            for day in rb_orders.index:
                for sym in syms:
                    if rb_orders.ix[day, sym] > 0:
                        order_list.append([day.date(), sym, 'BUY', 500])
                    elif rb_orders.ix[day, sym] < 0:
                        order_list.append([day.date(), sym, 'SELL', 500])
        if verbose:
            rule_stats_train = get_portfolio_stats(rb_df['Rule Based Portfolio'])
            print ",".join(str(x) for x in rule_stats_train)
            for order in order_list:
                print "   ".join(str(x) for x in order)
        if save_orders:
            if not os.path.exists("orders/"):
                os.makedirs("orders/")
            with open('orders/RB_orders.csv', mode='w') as rb_orders_file:
                for order in order_list:
                    rb_orders_file.write(",".join(str(x) for x in order)+"\n")
            rb_orders_file.close()
    if strategy == "all" or strategy == "ml":
        learner = bgl.BagLearner(ctl.CTLearner, kwargs={"leaf_size": 10}, bags=10)
        xdata, labels = ml.build_data(prices, syms, indicators, YBUY=0.01, YSELL=-0.01)
        learner.addEvidence(xdata, labels)

        ml_df, ml_orders = ml.ml_strategy(learner, xdata, prices, syms, verb=verbose)
        if verbose or save_orders:
            order_list = []
            for day in ml_orders.index:
                for sym in syms:
                    if ml_orders.ix[day, sym] > 0:
                        order_list.append([day.date(), sym, 'BUY', 500])
                    elif ml_orders.ix[day, sym] < 0:
                        order_list.append([day.date(), sym, 'SELL', 500])
        if verbose:
            ml_stats_train = get_portfolio_stats(ml_df)
            print ",".join(str(x) for x in ml_stats_train)
            for order in order_list:
                print "   ".join(str(x) for x in order)
        if save_orders:
            if not os.path.exists("orders/"):
                os.makedirs("orders/")
            with open("orders/ML_based_orders.csv", "w") as ml_orders_file:
                for order in order_list:
                    ml_orders_file.write(",".join(str(x) for x in order)+"\n")
            ml_orders_file.close()
    if strategy == "all" and verbose:
        df_temp = pd.concat([ml_df, rb_df['Rule Based Portfolio'], rb_df['Benchmark']],
                        keys=['ML based Portfolio', 'Rule based Portfolio', 'BENCHMARK'], axis=1)
        ml.plot_normalized_data(df_temp, ml_orders, "Daily portfolio value and Benchmark", "Date", "Normalized price")


    # TESTING
    stdate = dt.datetime(2010, 1, 1)
    enddate = dt.datetime(2010, 12, 31)  # just a few days for "shake out"
    syms = ["IBM"]

    dates = pd.date_range(start=stdate, end=enddate)
    test_prices = ut.get_data(syms, dates)  # automatically adds SPY
    # instantiate the strategy learner
    indicators = ind.Indicators(14)
    indicators.train(test_prices, syms, verbose=verbose)
    if verbose: print test_prices
    if strategy == "all" or strategy == "rule":
        rb_df_test, rb_orders_test = rule_based.rb_strategy(indicators, test_prices, syms, verb=verbose)
        if verbose or save_orders:
            # create orders list to save or print
            order_list = []
            for day in rb_orders_test.index:
                for sym in syms:
                    if rb_orders_test.ix[day, sym] > 0:
                        order_list.append([day.date(), sym, 'BUY', 500])
                    elif rb_orders_test.ix[day, sym] < 0:
                        order_list.append([day.date(), sym, 'SELL', 500])
        if verbose:
            rule_stats_test = get_portfolio_stats(rb_df_test['Rule Based Portfolio'])
            print ",".join(str(x) for x in rule_stats_test)
            # print orders list
            for order in order_list:
                print "   ".join(str(x) for x in order)
        if save_orders:
            with open('orders/RB_orders_test.csv', mode='w') as rb_orders_file:
                for order in order_list:
                    rb_orders_file.write(",".join(str(x) for x in order) + "\n")
            rb_orders_file.close()
    if strategy == "all" or strategy == "ml":
        test_data, ground_truth = ml.build_data(test_prices, syms, indicators)
        ml_df_test, ml_orders_test = ml.ml_strategy(learner, test_data, test_prices, syms, verb=verbose)
        if verbose or save_orders:
            order_list = []
            for day in ml_orders_test.index:
                for sym in syms:
                    if ml_orders_test.ix[day, sym] > 0:
                        order_list.append([day.date(), sym, 'BUY', 500])
                    elif ml_orders_test.ix[day, sym] < 0:
                        order_list.append([day.date(), sym, 'SELL', 500])
        # if verbose:
            # rmse1 = math.sqrt(((ground_truth - predY) ** 2).sum() / trainY.shape[0])
            # # print
            # # print "In sample results"
            # # print "RMSE: ", rmse
            # c1 = np.corrcoef(predY, y=trainY)
            # # print "corr: ", c[0,1]
            #
            # # evaluate out of sample
            # predY = learner.query(testX)  # get the predictions
            #
            # rmse2 = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
            # # print
            # # print "Out of sample results"
            # # print "RMSE: ", rmse
            # c2 = np.corrcoef(predY, y=testY)
        if verbose:
            ml_stats_test = get_portfolio_stats(ml_df_test)
            print ",".join(str(x) for x in ml_stats_test)
            for order in order_list:
                print "   ".join(str(x) for x in order)
        if save_orders:
            with open("orders/ML_based_orders_test.csv", "w") as ml_orders_file:
                for order in order_list:
                    ml_orders_file.write(",".join(str(x) for x in order) + "\n")
            ml_orders_file.close()
    if strategy == "all" and verbose:
        df_temp = pd.concat([ml_df_test, rb_df_test['Rule Based Portfolio'], rb_df_test['Benchmark']],
                            keys=['ML based Portfolio', 'Rule based Portfolio', 'BENCHMARK'], axis=1)
        ml.plot_normalized_data(df_temp, ml_orders, "Daily portfolio value and Benchmark", "Date", "Normalized price")


def get_portfolio_stats(port_val, daily_rf=0.0, samples_per_year=252):
    daily_return = port_val.copy()
    daily_return.ix[1:] = (port_val.ix[1:]/port_val.ix[:-1].values - 1)
    daily_return.ix[0] = 0
    daily_return_normed = daily_return.ix[1:]

    cr = (port_val[-1]/port_val[0])-1
    adr = daily_return_normed.mean()
    sddr = daily_return_normed.std()
    sr = np.sqrt(samples_per_year)*(adr-daily_rf)/sddr
    return cr, adr, sddr, sr


if __name__ == "__main__":
    paths = main(sys.argv[1:])
    run_strategy(paths[0], paths[1], paths[2])
