import numpy as np
from collections import Counter


class CTLearner:
    def __init__(self, leaf_size=1, verbose=False):
        self.leaf_size = leaf_size
        self.verbose = verbose
        self.tree = []
        self.leaf_index = -1

    # training step
    def addEvidence(self, Xtrain, Ytrain):
        Xshape = Xtrain.shape
        data = np.ones([Xshape[0], Xshape[1]+1])
        data[:, 0:Xshape[1]] = Xtrain
        data[:, -1] = Ytrain
        self.tree = self.buildCT(data)

    def buildCT(self, data):
        if len(data) <= 0:
            return np.array([[self.leaf_index, -1, -1, -1]])
        if data.shape[0] <= self.leaf_size:
            label = int(Counter(data[:, -1]).most_common(1)[0][0])
            return np.array([[self.leaf_index, label, -1, -1]])

        if np.all(data[:, -1] == data[0, -1]):
            return np.array([[self.leaf_index, data[0, -1], -1, -1]])
        split_feature = np.random.randint(0, data.shape[1]-1)
        split_value = (data[np.random.randint(0, data.shape[0]), split_feature]+data[np.random.randint(0, data.shape[0]), split_feature])/2.0
        left_tree = self.buildCT(data[data[:, split_feature] <= split_value])
        right_tree = self.buildCT(data[data[:, split_feature] > split_value])
        root = np.array([[split_feature, split_value, 1, left_tree.shape[0]+1]])
        tree = np.append(root, np.append(left_tree, right_tree, axis=0), axis=0)
        return tree

    # query
    def query(self, Xtest):
        Yresult = []
        for x in Xtest:
            index = 0
            while index < self.tree.shape[0] and self.tree[index, 0] != self.leaf_index:
                if x[self.tree[index, 0]] <= self.tree[index, 1]:
                    index += self.tree[index, 2]
                else:
                    index += self.tree[index, 3]
            Yresult.append(self.tree[index, 1])
        return Yresult
