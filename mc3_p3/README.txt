#########################
HOW TO LAUNCH THE PROGRAM
#########################

The script launch.py is given to begin the program
There are 3 parameters for this script:
    -s / --strategy: string  - "all", "rule", "ml" - default all
    -v / --verbose : boolean - plots all the graphs if True - default False
    -o / --orders  : boolean - saves the orders into the directory "orders/" if True - default False

s stands for the strategy to launch, by default, all strategies are launched. If you want to only study one strategy,
please enter "ml" or "rule" to launch either the Machine Learning or the Rule strategy.
v stands for verbose, if True, all the plots will be displayed, none otherwise
o stands for "save orders". If true, the program will save the orders in the "orders/" directory (it will be created if not already present)
Besides, the order files will be overwritten if they are already in the directory.

you can also type launch.py -h / --help
to see a short description of the parameters

Example of command:
    launch.py -s "all" -v False -o False
    equivalent to : "launch.py", just runs the strategies, does not display anything.

#########################
CONFIGURATION
#########################
Python 2.7.12
numpy  1.10.4
pandas 0.17.1
matplotlib 1.5.0
