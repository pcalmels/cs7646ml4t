"""
Test the Rule Based Strategy.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def rb_strategy(indicators, prices, syms, verb=True, start_price=100000):
    orders = rules(prices, syms, indicators)
    orders.index.rename('Date', inplace=True)
    port_vals = compute_portvals(orders, syms, prices, start_price)
    if isinstance(port_vals, pd.DataFrame):
        port_vals = port_vals[port_vals.columns[0]]  # just get the first column
    else:
        "warning, code did not return a DataFrame"
    orders_IBM = prices.copy()
    orders_IBM.ix[0, 'IBM'] = 500
    orders_IBM.ix[1:, :] = np.nan
    orders_IBM = orders_IBM.dropna()
    ibm_vals = compute_portvals(orders_IBM, syms, prices, start_price)
    df_temp = pd.concat([port_vals, ibm_vals], keys=['Rule Based Portfolio', 'Benchmark'], axis=1)
    if verb:
        plot_normalized_data(df_temp, orders, "Manual portfolio Daily value and Benchmark", "Date", "Normalized price")
        pass
    return df_temp, orders


def rules(prices, syms, indicator):
    ratio = indicator.ratio
    bbp = indicator.bbp
    rsi = indicator.rsi
    long_trend = indicator.longtrend
    medium_trend = indicator.mediumtrend
    # DECISION MAKING
    orders = prices[syms].copy()
    orders.ix[:, :] = np.NaN

    spy_rsi = rsi.copy()
    spy_rsi.values[:, :] = spy_rsi.ix[:, ['SPY']]

    sma_cross = pd.DataFrame(0, index=ratio.index, columns=ratio.columns)
    sma_cross[ratio >= 1] = 1

    sma_cross[1:] = sma_cross.diff()
    sma_cross.ix[0] = 0

    # CREATE ORDERS
    # GO LONG
    orders[  # (sma < 0.95) &
        (long_trend > 0)
        & (medium_trend > 0)
        & (rsi < 30)
        & (spy_rsi > 30)
        & (bbp < 0)] = 500

    # GO SHORT
    orders[  # (sma > 1.05) &
        (long_trend < 0)
        & (medium_trend < 0)
        & (rsi > 70)
        & (spy_rsi < 70)
        & (bbp > 0.8)] = -500

    orders[(sma_cross != 0)] = 0
    orders.ffill(inplace=True)
    orders.fillna(0, inplace=True)
    orders[1:] = orders.diff()
    orders.ix[0] = 0

    count = 0
    for i in range(len(orders.index)):
        inf_lim = max(i - 10, 0)
        if (500 in orders.iloc[i].abs().values) and np.any(orders.iloc[inf_lim:i].values != 0):
            orders.iloc[i, :] = 0
        else:
            new_count = count + (orders.iloc[i].sum())
            if abs(new_count) <= 500:
                count = new_count
            else:
                orders.iloc[i] = 0
    orders = orders.loc[(orders != 0).any(axis=1)]
    return orders


def compute_portvals(orders_df, symbols, prices, start_val = 100000):
    orders_df = orders_df[symbols]
    date_index = orders_df.index.unique()
    prices_df = prices[symbols]
    prices_df['CASH'] = np.ones(prices_df.shape[0])
    book_df = prices_df.copy()
    book_df[:] = np.zeros(prices_df.shape)
    book_df.ix[date_index, symbols] = orders_df
    book_df.CASH = (-1*prices_df[symbols]*book_df[symbols]).sum(axis=1)
    holdings_df = book_df.copy()
    holdings_df.ix[0, 'CASH'] += start_val
    holdings_df = holdings_df.cumsum(axis=0)
    portval = prices_df*holdings_df
    return portval.sum(axis=1)


def get_portfolio_stats(port_val, daily_rf, samples_per_year):
    daily_return = port_val.copy()
    daily_return.ix[1:] = (port_val.ix[1:]/port_val.ix[:-1].values - 1)
    daily_return.ix[0] = 0
    daily_return_normed = daily_return.ix[1:]

    cr = (port_val[-1]/port_val[0])-1
    adr = daily_return_normed.mean()
    sddr = daily_return_normed.std()
    sr = np.sqrt(samples_per_year)*(adr-daily_rf)/sddr
    return cr, adr, sddr, sr


def plot_normalized_data(df, orders, title, xlabel, ylabel):
    df_normed = df/df.ix[0, :].values
    ax = df_normed.plot(title=title, fontsize=12, color=['blue', 'black'])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    holdings = orders.cumsum()
    for i in orders.index:
        color = "black"
        if holdings.ix[i, 'IBM'] == 500:
            color = "g"
        if holdings.ix[i, 'IBM'] == -500:
            color = "r"
        if holdings.ix[i, 'IBM'] == 0:
            color = "black"
        plt.axvline(x=i, color=color, linewidth=1)
    plt.show()
