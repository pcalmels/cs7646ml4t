"""
Test the ML-Based Strategy.
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def ml_strategy(learner, data, prices, syms, verb=False, start_price=100000):
    # instantiate the strategy learner
    prices_IBM = prices[syms]
    prediction = np.array(learner.query(data))
    # last_transaction = 0
    count = 0
    for i in range(len(prediction)):
        inf_lim = max(i-10, 0)
        if (prediction[i] == 1 or prediction[i] == -1) and \
                (500 in prediction[inf_lim:i] or -500 in prediction[inf_lim:i]):
            prediction[i] = 0
        else:
            new_count = count + 500 * prediction[i]
            if abs(new_count) <= 500:
                count = new_count
                prediction[i] *= 500
            else:
                prediction[i] = 0
    orders = pd.DataFrame(prediction, index=prices_IBM.index, columns=syms)
    orders = orders.loc[(orders != 0).any(axis=1)]
    orders.index.rename('Date', inplace=True)
    port_vals = compute_portvals(orders, syms, prices, start_price)

    if isinstance(port_vals, pd.DataFrame):
        port_vals = port_vals[port_vals.columns[0]]  # just get the first column
    else:
        "warning, code did not return a DataFrame"
    if verb:
        orders_IBM = prices.copy()
        orders_IBM.ix[0, 'IBM'] = 500
        orders_IBM.ix[1:, :] = np.nan
        orders_IBM = orders_IBM.dropna()
        ibm_vals = compute_portvals(orders_IBM, syms, prices, start_price)
        df_temp = pd.concat([port_vals, ibm_vals], keys=['ML Based Portfolio', 'BENCHMARK'], axis=1)
        plot_normalized_data(df_temp, orders, "ML portfolio Daily value and Benchmark", "Date", "Normalized price", colors=['green', 'black'])
        pass

    return port_vals, orders


def build_data(prices_all, syms, indicators, window=10, YBUY=0.1, YSELL=-0.1):
    prices = prices_all[syms]
    window_ret = (prices.shift(-window)/prices) - 1.0
    window_ret = window_ret.ffill()#method="ffill")
    # Build labels based on future returns
    y_label = window_ret.as_matrix().reshape((prices.shape[0],))
    y_label[y_label < YSELL] = -1
    y_label[y_label > YBUY] = 1
    y_label[(y_label <= YBUY) & (y_label >= YSELL)] = 0

    # Build dataframe based on the indicators
    x_data = prices.copy()
    x_data['LONGTREND'] = indicators.longtrend[syms]
    x_data['MEDTREND'] = indicators.mediumtrend[syms]
    x_data['RATIO'] = indicators.ratio[syms]
    x_data['BBP'] = indicators.bbp[syms]
    x_data['RSI'] = indicators.rsi[syms]
    x_data = x_data.fillna(method="ffill")
    x_data = x_data.fillna(method="bfill")
    x_data = x_data.as_matrix()
    return x_data, y_label


def compute_portvals(orders_df, symbols, prices, start_val = 100000):
    # this is the function the autograder will call to test your code
    orders_df = orders_df[symbols]
    date_index = orders_df.index.unique()
    # prices_df = get_data(symbols, pd.date_range(date_index[0], date_index[-1]))
    prices_df = prices[symbols]  # remove SPY
    prices_df['CASH'] = np.ones(prices_df.shape[0])

    book_df = prices_df.copy()
    book_df[:] = np.zeros(prices_df.shape)
    book_df.ix[date_index, symbols] = orders_df
    book_df.CASH = (-1*prices_df[symbols]*book_df[symbols]).sum(axis=1)
    holdings_df = book_df.copy()
    holdings_df.ix[0, 'CASH'] += start_val #+ book_df.ix[0].CASH
    holdings_df = holdings_df.cumsum(axis=0)
    portval = prices_df*holdings_df
    return portval.sum(axis=1)


def get_portfolio_stats(port_val, daily_rf, samples_per_year):
    daily_return = port_val.copy()
    daily_return.ix[1:] = (port_val.ix[1:]/port_val.ix[:-1].values - 1)
    daily_return.ix[0] = 0
    daily_return_normed = daily_return.ix[1:]

    cr = (port_val[-1]/port_val[0])-1
    adr = daily_return_normed.mean()
    sddr = daily_return_normed.std()
    sr = np.sqrt(samples_per_year)*(adr-daily_rf)/sddr
    return cr, adr, sddr, sr


def plot_normalized_data(df, orders, title, xlabel, ylabel, colors=['green', 'blue', 'black']):
    df_normed = df/df.ix[0, :].values
    ax = df_normed.plot(title=title, fontsize=12, color=colors)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    holdings = orders.cumsum()
    for i in orders.index:
        color = "black"
        if holdings.ix[i, 'IBM'] == 500:
            color = "g"
        if holdings.ix[i, 'IBM'] == -500:
            color = "r"
        if holdings.ix[i, 'IBM'] == 0:
            color = "black"
        plt.axvline(x=i, color=color, linewidth=1)
    plt.show()

