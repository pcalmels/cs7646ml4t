"""
Build Technical Indicators.
"""

import numpy as np
import pandas as pd
import util as ut
import matplotlib.pyplot as plt

class Indicators(object):
    def __init__(self, lookback):
        self.lookback = lookback
        self.ratio = None
        self.bbp = None
        self.rsi = None
        self.longtrend = None
        self.mediumtrend = None

    def train(self, prices, symbols, verbose=True):
        # following the advice of http://www.investopedia.com/articles/forex/10/indicators-fx-traders-must-know.asp
        prices = prices/prices.ix[0, :].values
        #  MOVING AVERAGE / TREND (should replace the SMA)
        # SMA init
        sma = pd.rolling_mean(prices, window=self.lookback, min_periods=self.lookback)
        sma = sma.fillna(method='bfill')
        # CONFIRM MACD / RIBBON / OBV / ROC
        long_term_ma = pd.ewma(prices, com=self.lookback+2, min_periods=self.lookback+2)
        medium_term_ma = pd.ewma(prices, com=self.lookback, min_periods=self.lookback)
        short_term_ma = pd.ewma(prices, com=self.lookback/2.0, min_periods=self.lookback/2.0)
        medium_trend = np.sign(medium_term_ma - short_term_ma).fillna(method="bfill")
        long_trend = np.sign(long_term_ma - short_term_ma).fillna(method="bfill")


        if verbose:
            trendlinelong = long_trend.copy()
            trendlinelong = trendlinelong.fillna(method="ffill")
            trendlinelong = trendlinelong.fillna(method="bfill")
            # trend_line = global_trend.copy()['IBM']#.rename(columns={'IBM': 'trend'})['trend']

            plot_sma_df = pd.concat([short_term_ma['IBM'], medium_term_ma['IBM'], long_term_ma['IBM']],
                                    keys=[str(self.lookback/2.0) + ' EMA', str(self.lookback) + ' EMA', str(2*self.lookback) + ' EMA'],
                                    axis=1, join='inner', join_axes=[prices.index]).fillna(method='ffill').fillna(method='bfill')
            plot_sma_df = plot_sma_df/plot_sma_df.ix[0, :].values
            ax = plot_sma_df.plot(title='EMA Crosses', fontsize=12, color=['cyan', 'blue', 'black'])
            ax.set_xlabel('Date')
            ax.set_ylabel('Normed EMA value')
            new_trend = trendlinelong*trendlinelong.shift(1)
            del new_trend['SPY']
            new_trend = new_trend.fillna(0)
            l = -1
            for i in trendlinelong.index:
                if new_trend.ix[i, 'IBM'] == -1:
                    if trendlinelong.ix[i,'IBM'] >= 1:
                        plt.axvline(x=trendlinelong.index[l], color='red')
                    else:
                        plt.axvline(x=trendlinelong.index[l], color='green')
                l += 1
            plt.show()

        # FILTER / VOLATILITY
        # Bollinger Bands
        rolling_std = pd.rolling_std(prices, window=self.lookback, min_periods=self.lookback)
        top_band = sma + (2*rolling_std)
        bottom_band = sma - (2*rolling_std)
        # bbp = (prices - sma)/(2*rolling_std)
        bbp = (prices - bottom_band) / (top_band - bottom_band)
        bbp = bbp.fillna(method="bfill")
        if verbose:
            plot_bb_df = pd.concat([sma['IBM'], top_band['IBM'], bottom_band['IBM']],
                                    keys=['SMA', 'Top band', 'Bottom band'], axis=1, join='inner', join_axes=[prices.index])\
                .fillna(method='ffill').fillna(method='bfill')
            ax = plot_bb_df.plot(title='Bollinger Bands', fontsize=12, color=['purple', 'green', 'blue'])
            ax.set_xlabel('Date')
            ax.set_ylabel('value')
            plt.show()

            ax = bbp['IBM'].plot(title='Bollinger Bands ratio', fontsize = 12)
            ax.set_xlabel('Date')
            ax.set_ylabel('value')
            plt.axhline(y = 0.8, color='red')
            plt.axhline(y = 0, color='green')
            plt.show()

        ratio = prices/sma
        if verbose:
            plot_sma_df = pd.concat([prices['IBM'], sma['IBM']], keys=['Price', 'SMA'], axis=1, join='inner', join_axes=[prices.index]) \
                .fillna(method='ffill').fillna(method='bfill')
            ut.plot_data(plot_sma_df, title='IBM price and SMA', xlabel='Date', ylabel='normed value')
            ax = ratio['IBM'].plot(title='IBM Price/SMA Ratio', fontsize=12, color='purple')
            ax.set_xlabel('Date')
            ax.set_ylabel('Ratio value')
            plt.show()
        # OSCILLATOR / MOMENTUM
        daily_rets = prices.copy()
        daily_rets.values[1:, :] = prices.values[1:, :] - prices.values[:-1, :]
        daily_rets.values[0, :] = np.nan

        up_rets = daily_rets[daily_rets >= 0].fillna(0).cumsum()

        down_rets = -1*daily_rets[daily_rets < 0].fillna(0).cumsum()
        up_gain = prices.copy()
        up_gain.ix[:, :] = 0
        up_gain.values[self.lookback:, :] = up_rets.values[self.lookback:, :] - up_rets.values[:-self.lookback, :]

        down_loss = prices.copy()
        down_loss.ix[:, :] = 0
        down_loss.values[self.lookback:, :] = down_rets.values[self.lookback:, :] - down_rets.values[:-self.lookback, :]

        # rsi
        rs = (up_gain / self.lookback) / (down_loss / self.lookback)
        rsi = 100 - (100 / (1 + rs))
        rsi.ix[:self.lookback, :] = np.nan
        rsi[rsi == np.inf] = 100
        rsi = rsi.fillna(method='bfill')
        if verbose:
            plot_returns = pd.concat([up_gain['IBM'], down_loss['IBM']], keys=['up gain', 'down loss'],axis=1, join='inner', join_axes=[prices.index]) \
                .fillna(method='ffill').fillna(method='bfill')
            ut.plot_data(plot_returns, title='IBM possible Gain and Loss', xlabel='Date', ylabel='normed value')
            ax = rsi['IBM'].plot(title='RSI', fontsize=12, color='purple')
            ax.set_xlabel('Date')
            ax.set_ylabel('RSI value')
            plt.axhline(y=30, color='green')
            plt.axhline(y=70, color='red')
            plt.show()
        self.ratio = ratio
        self.bbp = bbp
        self.rsi = rsi
        self.longtrend = long_trend
        self.mediumtrend = medium_trend

