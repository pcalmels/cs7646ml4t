import numpy as np
import pandas as pd
import math


class Indicators(object):
    def __init__(self, start_date, end_date, lookback):
        self.start_date = start_date
        self.end_date = end_date
        self.lookback = lookback
        self.sma = None
        self.bbp = None
        self.rsi = None

    def train(self, prices, symbols):
        sma = prices.copy()
        bbp = prices.copy()
        rsi = prices.copy()
        for day in range(prices.shape[0]):
            rsi.ix[day, :] = 0
        for day in range(prices.shape[0]):
            if day < self.lookback:
                sma.ix[day, :] = np.nan
                bbp.ix[day, :] = np.nan
                rsi.ix[day, :] = np.nan
                continue
            sma.ix[day, :] = prices.ix[day-self.lookback+1:day+1, :].sum()
        for sym in symbols:
            sma.ix[day, sym] = prices.ix[day-self.lookback+1:day+1, sym].sum()
        for prev_day in range(day-self.lookback+1, day+1):
            sma.ix[day, sym] += prices.ix[prev_day, sym]
        sma.ix[day, sym] = sma.ix[day, sym] / self.lookback

        sma.ix[day, :] = prices.ix[day - self.lookback + 1:day + 1, :].sum(axis=0) / self.lookback
        sma.values[self.lookback:, :] = (sma.values[self.lookback] - sma.values[:-self.lookback, :]) / self.lookback
        sma.ix[:self.lookback, :] = np.nan

        for day in range(prices.shape[0]):
            for sym in symbols:
                bbp.ix[day, sym] = ((prices.ix[day-self.lookback+1:day+1, sym] - sma.ix[day, sym]) ** 2).sum()
                # for prev_day in range(day-self.lookback+1, day+1):
                #     bbp.ix[day, sym] += math.pow(prices.ix[prev_day, sym] - sma.ix[day, sym], 2)
                bbp.ix[day, sym] = math.sqrt(bbp.ix[day, sym] / (self.lookback - 1))

                bottom_band = sma.ix[day, sym] - (2 * bbp.ix[day, sym])
                top_band = sma.ix[day, sym] + (2 * bbp.ix[day, sym])
                bbp.ix[day, sym] = (prices.ix[day, sym] - bottom_band) / (top_band - bottom_band)

        for day in range(prices.shape[0]):
            for sym in symbols:
                up_gain = 0
                down_loss = 0
                for prev_day in range(day - self.lookback + 1, day + 1):
                    delta = prices.ix[prev_day, sym] - prices.ix[prev_day-1, sym]
                    if delta >= 0:
                        up_gain += delta
                    else:
                        down_loss -= delta
                    if down_loss == 0:
                        rsi.ix[day, sym] = 100
                    else:
                        rs = (up_gain/self.lookback) / (down_loss/self.lookback)
                        rsi.ix[day, sym] = 100 - (100/(1+rs))

                for sym in symbols:
                    if down_loss[sym] ==0:
                        rsi.ix[day, sym] = 100
                    else:
                        rs = (up_gain[sym] / self.lookback) / (down_loss[sym] / self.lookback)
                        rsi.ix[day, sym] = 100 - (100/(1+rs))

        self.sma = sma
        self.bbp = bbp
        self.rsi = rsi

    def test(self, prices, symbols):
        orders = []
        holdings = {sym: 0 for sym in symbols}
        for day in range(self.lookback + 1, prices.shape[0]):
            for sym in symbols:
                if sym == 'SPY':
                    continue
                if (self.sma.ix[day, sym] < 0.95) \
                        and (self.bbp.ix[day, sym] < 0) \
                        and (self.rsi.ix[day, sym] < 30) and (self.rsi.ix[day, 'SPY'] > 30):
                    # Maybe oversold
                    if holdings[sym] < 100:
                        holdings[sym] += 100
                        orders.append([prices.index[day].date(), sym, 'BUY', 100])
                elif (self.sma.ix[day, sym] > 1.05) \
                        and (self.bbp.ix[day, sym] > 1) \
                        and (self.rsi.ix[day, sym] > 70) and (self.rsi.ix[day, 'SPY'] < 70):
                    # Overbought?
                    if holdings[sym] > -100:
                        holdings[sym] -= 100
                        orders.append([prices.index[day].date(), sym, 'SELL', 100])
                elif (self.sma.ix[day, sym] >= 1) \
                        and (self.bbp.ix[day - 1, sym] < 1) \
                        and (holdings[sym] > 0):
                    # Close long position
                    holdings[sym] = 0
                    orders.append([prices.index[day].date(), sym, 'SELL', 100])
                elif (self.sma.ix[day, sym] <= 1) \
                        and (self.bbp.ix[day - 1, sym] > 1) \
                        and (holdings[sym] < 0):
                    # Close short position
                    holdings[sym] = 0
                    orders.append([prices.index[day].date(), sym, 'BUY', 100])
        return orders


