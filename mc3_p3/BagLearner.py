import numpy as np
from collections import Counter

class BagLearner:
    def __init__(self, learner, kwargs={"leaf_size": 1}, bags=20, boost=False, verbose=False):
        self.learner = learner
        self.kwargs = kwargs
        self.bags = bags
        self.boost = boost
        self.verbose = verbose
        self.learners = []

    def addEvidence(self, Xtrain, Ytrain):
        data = np.ones([Xtrain.shape[0], Xtrain.shape[1] + 1])
        data[:, 0:Xtrain.shape[1]] = Xtrain
        data[:, -1] = Ytrain
        kwargs = self.kwargs
        shape = data.shape
        learners = []
        for i in range(0, self.bags):
            learners.append(self.learner(**kwargs))
        if self.boost:
            return -1
        else:
            for learner in learners:
                BaggedData = np.array([data[np.random.randint(0, shape[0]), :]])
                random_indices = np.random.randint(0, shape[0], size=shape[0]-1)
                for l in random_indices:
                    BaggedData = np.append(BaggedData, [data[l, :]], axis=0)
                learner.addEvidence(BaggedData[:, :-1], BaggedData[:, -1])
            self.learners = learners

    def query(self, Xtest):
        total_labels = np.zeros(Xtest.shape[0])
        for learner in self.learners:
            total_labels = np.add(total_labels, learner.query(Xtest))
        total_labels[total_labels > 0] = 1
        total_labels[total_labels < 0] = -1
        total_labels[total_labels == 0] = 0
        return total_labels
