"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
"""

import datetime as dt
import QLearner as ql
import pandas as pd
import util as ut
import indicators as ind
import numpy as np

class StrategyLearner(object):

    # constructor
    def __init__(self, verbose = False):
        self.verbose = verbose
        self.position = 0

    # this method should create a QLearner, and train it for trading
    def addEvidence(self, symbol = "IBM", \
                    sd=dt.datetime(2008,1,1), \
                    ed=dt.datetime(2009,1,1), \
                    sv = 10000):

        self.learner = ql.QLearner()
        # add your code to do learning here
        # symbol = "ML4T-220"
        # example usage of the old backward compatible util function
        syms=[symbol]
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        prices = prices_all[syms]  # only portfolio symbols
        prices_SPY = prices_all['SPY']  # only SPY, for comparison later
        if self.verbose: print prices

        # example use with new colname 
        volume_all = ut.get_data(syms, dates, colname = "Volume")  # automatically adds SPY
        volume = volume_all[syms]  # only portfolio symbols
        volume_SPY = volume_all['SPY']  # only SPY, for comparison later
        if self.verbose: print volume


        indicators = ind.Indicators(2)
        indicators.train(prices_all, syms, self.verbose)
        # TODO Build states - DONE HERE TO GET THE MAXIMUM NUMBER OF STATES
        states, num_states = discretize(indicators, prices, syms)
        normed_prices = prices[syms] / prices.ix[0, syms].values
        print num_states
        qlearn = ql.QLearner(num_actions=3, num_states=num_states)
        count = 0
        orders = prices.copy()
        orders.ix[:, :] = 0
        orders['CASH'] = np.ones(len(prices.index))*sv
        print states
        cut_dates = prices.index.unique()
        last_reward = 0
        while True:

            # TODO INIT
            prev_date = cut_dates[0]
            X = states.loc[prev_date, 'state']
            a = qlearn.querysetstate(X)
            print a

            current_reward = 0
            # TODO TRAIN
            for d in cut_dates:
                r = self.get_reward(a, prev_date, d, normed_prices)
                current_reward += r
                print r
                print X
                a = qlearn.query(X, r)
                prev_date = d
                # TODO CHANGE THE ORDERS AND SELF.POSITION
                orders.loc[d, syms] = a
                new_pos = 0

                X = states.loc[d, 'state']
            # TODO CHANGE CONVERGENCE STATUS
            if (count < 10000) | (abs(current_reward - last_reward)<10):
                break
            else:
                last_reward = current_reward
                count += 1
                # states, n = discretize(indicators, prices, syms)
            print last_reward

    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "IBM", \
                   sd=dt.datetime(2009,1,1), \
                   ed=dt.datetime(2010,1,1), \
                   sv = 10000):

        # here we build a fake set of trades
        # your code should return the same sort of data
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data([symbol], dates)  # automatically adds SPY
        trades = prices_all[[symbol,]]  # only portfolio symbols
        trades_SPY = prices_all['SPY']  # only SPY, for comparison later
        trades.values[:,:] = 0 # set them all to nothing
        trades.values[3,:] = 500 # add a BUY at the 4th date
        trades.values[5,:] = -500 # add a SELL at the 6th date
        trades.values[6,:] = -500 # add a SELL at the 7th date
        trades.values[8,:] = 1000 # add a BUY at the 9th date
        if self.verbose: print type(trades) # it better be a DataFrame!
        if self.verbose: print trades
        if self.verbose: print prices_all
        return trades


    def get_reward(self, action, previous_date, current, prices):
        new_position = 0
        # BUY
        if action == 1:
            if self.position != 1:
                new_position = self.position + 1
        # SELL
        elif action == 2:
            if self.position != -1:
                new_position = self.position - 1
        else:
            pass
        r = new_position * (float(prices.ix[current, :])/float(prices.ix[previous_date, :]) - 1)
        return r


def discretize(indicators, prices, syms):
    indic_df = prices.copy()
    for s in syms:
        del indic_df[s]
    indic_df['ratio'] = indicators.ratio[syms]
    indic_df['bbp'] = indicators.bbp[syms]
    indic_df['rsi'] = indicators.rsi[syms]

    step = 1
    for col in indic_df:
        indic_df[col] = pd.cut(indic_df[col], 10, labels=range(0, 10*step, step), include_lowest=True)
        step *= 10
    indic_df['long_trend'] = indicators.longtrend[syms]
    indic_df['medium_trend'] = indicators.mediumtrend[syms]
    indic_df['long_trend'] = pd.cut(indic_df['long_trend'], 3, labels=range(0, 3*step, step), include_lowest=True)
    step *= 10
    indic_df['medium_trend'] = pd.cut(indic_df['medium_trend'], 3, labels=range(0, 3*step, step), include_lowest=True)
    indic_df['state'] = indic_df.sum(axis=1)
    return indic_df, 3*step



if __name__=="__main__":
    print "One does not simply think up a strategy"
